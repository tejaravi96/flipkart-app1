package com.flipkart.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.hamcrest.Matchers;

import com.flipkart.bean.BeanSteps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class SelectedResultTestPageSteps
		extends
			WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.ipadname.selectedpage")
	private QAFWebElement xpathIpadnameSelectedpage;

	@FindBy(locator = "xpath.ipadprice.selectedpage")
	private QAFWebElement xpathIpadpriceSelectedpage;

	@FindBy(locator = "xpath.addtocartbutton.selectedpage")
	private QAFWebElement xpathAddtocartbuttonSelectedpage;

	@FindBy(locator = "xpath.cartbutton.selctedpage")
	private QAFWebElement idCartbuttonSelctedpage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathIpadnameSelectedpage() {
		return xpathIpadnameSelectedpage;
	}

	public QAFWebElement getXpathIpadpriceSelectedpage() {
		return xpathIpadpriceSelectedpage;
	}

	public QAFWebElement getXpathAddtocartbuttonSelectedpage() {
		return xpathAddtocartbuttonSelectedpage;
	}

	public QAFWebElement getIdCartbuttonSelctedpage() {
		return idCartbuttonSelctedpage;
	}

	String ipadNameSelectedProduct = xpathIpadnameSelectedpage.getText();
	String ipadPriceSelectedProduct = xpathIpadpriceSelectedpage.getText();

	@QAFTestStep(description = "Verified product with stored values add to cart")
	public void addingProductToCart() {

		BeanSteps bean = new BeanSteps();
		System.out.println(bean.getMobileName());
		Validator.verifyThat(ipadNameSelectedProduct,
				Matchers.containsString(bean.getMobileName()));
		Reporter.log("Validated Selected Product");
		click("xpath.addtocartbutton.selectedpage");
		click("xpath.cartbutton.selctedpage");

	}

}
