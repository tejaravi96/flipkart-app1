package com.flipkart.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;



import java.util.List;

import org.hamcrest.Matchers;

import com.flipkart.bean.BeanSteps;
import com.flipkart.component.ComponetTestPageSteps;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

public class SearchResultTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.probutton.searchresultpage")
	private QAFWebElement xpathProbuttonSearchresultpage;

	@FindBy(locator = "id.filterbutton.searchresultpage")
	private QAFWebElement idFilterbuttonSearchresultpage;

	@FindBy(locator = "xpath.morebutton.filterresults.searchresultspage")
	private QAFWebElement xpathMorebuttonFilterresultsSearchresultspage;

	@FindBy(locator = "xpath.pricebutton.filtersearchresultpage")
	private QAFWebElement xpathPricebuttonFiltersearchresultpage;

	@FindBy(locator = "xpath.osbutton.searchresultspage")
	private QAFWebElement xpathOsbuttonSearchresultspage;

	@FindBy(locator = "id.oscheckbox.searchresultspage")
	private QAFWebElement idOscheckboxSearchresultspage;

	@FindBy(locator = "id.applybutton.flitersearchpage")
	private QAFWebElement idApplybuttonFlitersearchpage;

	@FindBy(locator = "id.sortbybutton.serachresultpage")
	private QAFWebElement idSortbybuttonSerachresultpage;

	@FindBy(locator = "xpath.sortbyvalue.searchresultpage")
	private QAFWebElement xpathSortbyvalueSearchresultpage;

	@FindBy(locator = "xpath.brandbutton.filtersaerchresultpage")
	private QAFWebElement xpathBrandbuttonFiltersaerchresultpage;

	@FindBy(locator = "xpath.applebrandcheckbox.filtersearchresultpage")
	private QAFWebElement xpathApplebrandcheckboxFiltersearchresultpage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathProbuttonSearchresultpage() {
		return xpathProbuttonSearchresultpage;
	}

	public QAFWebElement getIdFilterbuttonSearchresultpage() {
		return idFilterbuttonSearchresultpage;
	}

	public QAFWebElement getXpathMorebuttonFilterresultsSearchresultspage() {
		return xpathMorebuttonFilterresultsSearchresultspage;
	}

	public QAFWebElement getXpathPricebuttonFiltersearchresultpage() {
		return xpathPricebuttonFiltersearchresultpage;
	}

	public QAFWebElement getXpathOsbuttonSearchresultspage() {
		return xpathOsbuttonSearchresultspage;
	}

	public QAFWebElement getIdOscheckboxSearchresultspage() {
		return idOscheckboxSearchresultspage;
	}

	public QAFWebElement getIdApplybuttonFlitersearchpage() {
		return idApplybuttonFlitersearchpage;
	}

	public QAFWebElement getIdSortbybuttonSerachresultpage() {
		return idSortbybuttonSerachresultpage;
	}

	public QAFWebElement getXpathSortbyvalueSearchresultpage() {
		return xpathSortbyvalueSearchresultpage;
	}

	public QAFWebElement getXpathBrandbuttonFiltersaerchresultpage() {
		return xpathBrandbuttonFiltersaerchresultpage;
	}

	public QAFWebElement getXpathApplebrandcheckboxFiltersearchresultpage() {
		return xpathApplebrandcheckboxFiltersearchresultpage;
	}

	@FindBy(locator = "xpath.ipadcomponent.searchresultspage")
	private List<ComponetTestPageSteps> xpathIpadcomponentSearchresultspage;

	public List<ComponetTestPageSteps> getXpathIpadcomponentSearchresultspage() {
		return xpathIpadcomponentSearchresultspage;
	}

	@QAFTestStep(description = "Filter the result and sort by {0} price")
	public void filterByOS(String filterBy) {

		// click("xpath.probutton.searchresultpage");
		Reporter.log("Clicked on pro button");

		click("id.filterbutton.searchresultpage");

		// click("xpath.morebutton.filterresults.searchresultspage");

		click("xpath.brandbutton.filtersaerchresultpage");

		click("xpath.applebrandcheckbox.filtersearchresultpage");

		click("id.applybutton.flitersearchpage");

		click("id.sortbybutton.serachresultpage");
		Reporter.log("Clicked on sort by");

		QAFExtendedWebElement element =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("xpath.sortbyvalue.searchresultpage"), filterBy));
		element.click();
		Reporter.log("Selected sorted value");

		System.out.println("Selected pro filtered with os sorted by price");
	}

	@QAFTestStep(description = "Verify search results")
	public void verifySearchResults() {

		TouchAction action;
		action = new TouchAction((AppiumDriver<?>) new WebDriverTestBase().getDriver()
				.getUnderLayingDriver());

		action.waitAction().tap(525, 222).moveTo(557, 858);

		System.out.println("Scrolled");

		int size = xpathIpadcomponentSearchresultspage.size();
		System.out.println("Total results:" + size);

		Validator.verifyThat(getXpathIpadcomponentSearchresultspage().size(),
				Matchers.greaterThan(0));
		System.out.println("Validates greater then zero");

		try {
			for (ComponetTestPageSteps component : getXpathIpadcomponentSearchresultspage()) {

				Reporter.log("Mobile Name:"
						+ component.getXpathIpadnameSearchresultspage().getText());
				Reporter.log("Colour:"
						+ component.getXpathIpadcolorSearchresultpage().getText());
				Reporter.log("FlightPrice:"
						+ component.getXpathIpadpriceSearchresultspage().getText());

			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}
	
	@QAFTestStep(description = "Select elemets to  store the values {0}")
	public void beanVerify(int index) {

		BeanSteps step = new BeanSteps();

		BeanSteps.setMobileName(getXpathIpadcomponentSearchresultspage().get(index)
				.getXpathIpadnameSearchresultspage().getText());
		step.setColour(getXpathIpadcomponentSearchresultspage().get(index)
				.getXpathIpadcolorSearchresultpage().getText());
		step.setPrice(getXpathIpadcomponentSearchresultspage().get(index)
				.getXpathIpadpriceSearchresultspage().getText());
		step.info();
		xpathIpadcomponentSearchresultspage.get(index).click();
		Reporter.log("Clicked on index product");
	}

}
