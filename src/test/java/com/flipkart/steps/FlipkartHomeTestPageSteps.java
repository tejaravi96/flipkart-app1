package com.flipkart.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlipkartHomeTestPageSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.catagorybutton.flipkarthomepage")
	private QAFWebElement xpathCatagorybuttonFlipkarthomepage;

	@FindBy(locator = "xpath.electronics.homepage")
	private QAFWebElement xpathElectronicsHomepage;

	@FindBy(locator = "xpath.searchbox.homepage")
	private QAFWebElement xpathSearchboxHomepage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathCatagorybuttonFlipkarthomepage() {
		return xpathCatagorybuttonFlipkarthomepage;
	}

	public QAFWebElement getXpathSearchboxHomepage() {
		return xpathSearchboxHomepage;
	}

	public QAFWebElement getXpathElectronicsHomepage() {
		return xpathElectronicsHomepage;
	}

	@QAFTestStep(description = "Select the catagory")
	public void selectCatagory() {

		click("xpath.searchbox.homepage");

		System.out.println("Clciked on Search box");
		Reporter.log("Clciked on search box");
	}

}
