package com.flipkart.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;


import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class CatagoryTestPageSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.mobilesbutton.catogorypage")
	private QAFWebElement xpathMobilesbuttonCatogorypage;

	@FindBy(locator = "id.searchbox.catogorypage")
	private QAFWebElement idSearchboxCatogorypage;

	@FindBy(locator = "xpath.searchbutton.catagorypage")
	private QAFWebElement xpathSearchbuttonCatagorypage;

	@FindBy(locator = "xpath.appleipadoption.catagorypage")
	private QAFWebElement xpathAppleipadoptionCatagorypage;

	@FindBy(locator = "xpath.allowbutton.catagorypage")
	private QAFWebElement idAllowbuttonCatagorypage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathMobilesbuttonCatogorypage() {
		return xpathMobilesbuttonCatogorypage;
	}

	public QAFWebElement getIdSearchboxCatogorypage() {
		return idSearchboxCatogorypage;
	}

	public QAFWebElement getXpathSearchbuttonCatagorypage() {
		return xpathSearchbuttonCatagorypage;
	}

	public QAFWebElement getXpathAppleipadoptionCatagorypage() {
		return xpathAppleipadoptionCatagorypage;
	}

	public QAFWebElement getIdAllowbuttonCatagorypage() {
		return idAllowbuttonCatagorypage;
	}

	@QAFTestStep(description = "On search page search for apple ipad")

	public void selectMobiles() {

		sendKeys("apple ipad", "id.searchbox.catogorypage");
		System.out.println("sending apple");
		Reporter.log("Passed value into serach box");

		click("xpath.appleipadoption.catagorypage");
		System.out.println("Selected displayed option");
		Reporter.log("Selected displayed option");

		click("xpath.allowbutton.catagorypage");
		Reporter.log("Clicked on allow ");
	}

}
