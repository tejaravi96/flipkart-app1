package com.flipkart.steps;

import com.flipkart.bean.BeanSteps;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class CartTestPageSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.ipadname.cartpage")
	private QAFWebElement xpathIpadnameCartpage;

	@FindBy(locator = "xpath.continuebutton.cartpage")
	private QAFWebElement xpathContinuebuttonCartpage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathIpadnameCartpage() {
		return xpathIpadnameCartpage;
	}

	public QAFWebElement getXpathContinuebuttonCartpage() {
		return xpathContinuebuttonCartpage;
	}

	String ipadInCartPage = xpathIpadnameCartpage.getAttribute("name");
	@QAFTestStep(description = "Verify product in cart with actual and continue")
	public void verifyProductInCart() {
		System.out.println(ipadInCartPage);
		BeanSteps cartProduct = new BeanSteps();
		Validator.verifyThat(ipadInCartPage,
				Matchers.containsString(cartProduct.getMobileName()));
		click("xpath.continuebutton.cartpage");
	}
}
