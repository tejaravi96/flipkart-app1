package com.flipkart.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import com.qmetry.qaf.automation.step.QAFTestStep;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FlipkartLaunchTestPageSteps
		extends
			WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath.catagorybutton.flipkarthomepage")
	private QAFWebElement xpathCatagorybuttonFlipkarthomepage;

	@FindBy(locator = "id.closebutton.flipkartlaunchpage")
	private QAFWebElement idClosebuttonFlipkartlaunchpage;

	@FindBy(locator = "xpath.allowbutton.launchpage")
	private QAFWebElement xpathAllowbuttonLaunchpage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getXpathCatagorybuttonFlipkarthomepage() {
		return xpathCatagorybuttonFlipkarthomepage;
	}

	public QAFWebElement getIdClosebuttonFlipkartlaunchpage() {
		return idClosebuttonFlipkartlaunchpage;
	}

	public QAFWebElement getIdAllowbuttonLaunchpage() {
		return xpathAllowbuttonLaunchpage;
	}

	@QAFTestStep(description = "Launch the flipkart application")
	public void launch() {

		click("xpath.allowbutton.launchpage");
		System.out.println("Clicked on allow");
		Reporter.log("Clicked on allow");

		click("id.closebutton.flipkartlaunchpage");
		System.out.println("Clicked on closebutton");
		Reporter.log("Cliked on close button");
	}

}
