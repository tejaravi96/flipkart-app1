package com.flipkart.component;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ComponetTestPageSteps extends QAFWebComponent {

	public ComponetTestPageSteps(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "xpath.ipadprice.searchresultspage")
	private QAFWebElement xpathIpadpriceSearchresultspage;

	@FindBy(locator = "xpath.ipadcolor.searchresultpage")
	private QAFWebElement xpathIpadcolorSearchresultpage;

	@FindBy(locator = "xpath.ipadname.searchresultspage")
	private QAFWebElement xpathIpadnameSearchresultspage;

	public QAFWebElement getXpathIpadpriceSearchresultspage() {
		return xpathIpadpriceSearchresultspage;
	}

	public QAFWebElement getXpathIpadcolorSearchresultpage() {
		return xpathIpadcolorSearchresultpage;
	}

	public QAFWebElement getXpathIpadnameSearchresultspage() {
		return xpathIpadnameSearchresultspage;
	}

}
