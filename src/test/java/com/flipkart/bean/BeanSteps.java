package com.flipkart.bean;


import com.qmetry.qaf.automation.util.Reporter;

public class BeanSteps {

	String colour;
	static String mobileName;
	String price;

	public String getMobileName() {
		return mobileName;
	}

	public static void setMobileName(String mobileName) {
		BeanSteps.mobileName = mobileName;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void info() {
		Reporter.log("My info:" + mobileName + "," + colour + "," + price);
		System.out.println("My info:" + mobileName + "," + colour + "," + price);
	}

}
